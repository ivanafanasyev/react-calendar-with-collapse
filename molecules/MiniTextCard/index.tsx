import { css } from "@emotion/core";

export default function MiniTextCard(props) {
  return (
    <div className="text-mini-card">
      <div css={styles.param}>{props.param}</div>
      <div css={styles.value}>{props.value}</div>
    </div>
  );
}

const styles = {
  param: css`
    font-size: 16px;
    font-weight: 600;
    line-height: 1.56;
    color: #242220;
    margin-bottom: 8px;

    @media screen and (max-width: 602px) {
      font-size: 12px;
    }
  `,
  value: css`
    font-size: 14px;
    line-height: 1.5;
    color: #242220;

    @media screen and (max-width: 602px) {
      font-size: 12px;
    }
  `
};
