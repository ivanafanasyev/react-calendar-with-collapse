import styled from "@emotion/styled";
import dayjs from "dayjs";
import Collapse from "react-css-collapse";
import typography from "../../../../../../utils/typography";
import variables from "../../../../../../utils/variables";
import dropUrl from "./down.svg";
import expandUrl from "./up.svg";

import Panel from "../../atoms/Panel";
import Tag from "../../../../ui/atoms/Tag";
import MiniTextCard from "../MiniTextCard";

export default function ViewTab({
  time,
  statusData,
  statusType,
  name,
  expandState,
  expand
}) {
  return (
    <Panel className="view-tab">
      <ViewTabHeader onClick={expand} className="view-tab-header">
        <div className="view-tab-title">{dayjs(time).format("hh:mm A")}</div>
        <div className="view-tab-statuses">
          <Tag status={statusData} />
        </div>
        <img
          className="view-drop-wrap"
          src={!expandState ? dropUrl : expandUrl}
        />
      </ViewTabHeader>
      <Description className="view-tab-description">{name}</Description>
      <Collapse
        isOpen={expandState}
        transition={`${variables.animations.transitionCollapse}`}
      >
        <ViewTabData className="view-tab-data">
          <MiniTextCard param={"Property for sale"} value={"Yes"} />
          <MiniTextCard param={"Status"} value={"On the Market"} />
          <MiniTextCard param={"Payment type"} value={"Procedes from sale"} />
        </ViewTabData>
      </Collapse>
    </Panel>
  );
}

const ViewTabHeader = styled.div`
  display: grid;
  grid-template-areas: "title title statuses drop";
  grid-template-columns: auto auto auto 48px;
  align-items: center;
  grid-template-rows: 48px;

  .view-tab-title {
    grid-area: title;
    ${typography.body1_m};
    font-weight: bold;
  }

  .view-tab-statuses {
    margin-left: auto;
    display: grid;
    grid-template-columns: auto;
    -webkit-column-gap: 80px;
    column-gap: 80px;
    padding-right: 11px;

    @media screen and (max-width: 620px) {
      padding-right: 0;
    }
  }

  .view-drop-wrap {
    grid-area: drop;
    width: 48px;
    height: 48px;
    margin-left: auto;
    cursor: pointer;

    @media screen and (max-width: 620px) {
      width: 32px;
      height: 32px;
    }
  }

  @media screen and (max-width: 620px) {
    grid-template-columns: auto auto auto 32px;
  }
`;

const Description = styled.div`
  ${typography.body3_m};
`;

const ViewTabData = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-column-gap: 48px;
  grid-template-rows: auto;
  padding-top: 24px;
  padding-bottom: 33px;

  @media screen and (max-width: 620px) {
    grid-template-columns: repeat(2, 1fr);
    grid-template-rows: auto auto;
    grid-row-gap: 16px;
  }
`;
