import styled from "@emotion/styled";
import MiscDayName from "../../atoms/MiscDayName";

const WeekdaysShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

export default function Weekdays() {
  return (
    <Week className="weekdays">
      {WeekdaysShort.map(day => (
        <MiscDayName name={day} key={day} />
      ))}
    </Week>
  );
}

const Week = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: center;
  padding: 8px 52px;

  @media screen and (max-width: 602px) {
    padding: 6px 6px;
  }
`;
