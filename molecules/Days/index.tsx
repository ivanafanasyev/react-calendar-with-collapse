import styled from "@emotion/styled";
import dayjs from "dayjs";
import MiscDateNumber from "./../../atoms/MiscDateNumber";
import typography from "../../../../../../utils/typography";

import ViewTab from "../ViewTab";

function inThisDay(date, compareDate) {
  return dayjs(date).isSame(dayjs(compareDate), "day");
}

export default function Days({
  currentDate,
  selectedDate,
  todayDate,
  handleClickedDay,
  rowUnderLine,
  viewings,
  viewingsByDate,
  viewingsOpenIndex,
  toggle
}) {
  const monthStart = dayjs(currentDate).startOf("month");
  const monthEnd = dayjs(monthStart).endOf("month");
  const startDate = dayjs(monthStart).startOf("week");
  const endDate = dayjs(monthEnd).endOf("week");
  const rows = [];

  let days = [];
  let day = startDate;

  for (let j = 0; day <= endDate; j++) {
    for (let i = 0; i < 7; i++) {
      let params = viewings.filter(el => inThisDay(el.time, day));
      days.push(
        <MiscDateNumber
          date={day}
          disable={!dayjs(day).isSame(dayjs(monthStart), "month")}
          selected={dayjs(day).isSame(dayjs(selectedDate), "day")}
          today={dayjs(day).isSame(dayjs(todayDate), "day")}
          params={params}
          key={dayjs(day)
            .add(1, "day")
            .toString()}
        />
      );
      day = dayjs(day).add(1, "day");
    }

    rows.push(
      <div key={dayjs(day).toString()}>
        <Row className="row" onClick={e => handleClickedDay(e, j)}>
          {" "}
          {days}
        </Row>
        {rowUnderLine === j && (
          <ShowParamsLine className="show-params">
            <div className="show-params-date">
              {dayjs(selectedDate).format("ddd D MMM YYYY")}
            </div>
            <div className="viewings-container">
              {viewingsByDate.map(viewItem => (
                <ViewTab
                  time={viewItem.time}
                  name={viewItem.name}
                  key={viewItem._id}
                  statusType={viewItem.statusType}
                  statusData={viewItem.statusData}
                  expandState={viewingsOpenIndex === viewItem._id}
                  expand={() => toggle(viewItem._id)}
                />
              ))}
            </div>
          </ShowParamsLine>
        )}
      </div>
    );

    days = [];
  }

  return <DaysContainer>{rows}</DaysContainer>;
}

const Row = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: flex-start;
  padding: 0 52px;

  @media screen and (max-width: 602px) {
    padding: 0 6px;
  }
`;

const ShowParamsLine = styled.div`
  width: 100%;
  background: #ddf2ff;
  padding: 24px 0;

  .show-params-date {
    ${typography.body1_m};
    font-weight: bold;
    margin-bottom: 16px;
    margin-left: 16px;
  }

  .viewings-container {
    display: grid;
    grid-template-columns: 1fr;
    grid-template-rows: auto;
    grid-row-gap: 8px;
  }
`;

const DaysContainer = styled.div`
  display: grid;
  grid-template-rows: auto;
  grid-row-gap: 5px;
  padding-bottom: 8px;

  @media screen and (max-width: 620px) {
    padding-bottom: 6px;
  }
`;
