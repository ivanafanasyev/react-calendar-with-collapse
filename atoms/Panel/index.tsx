import styled from "@emotion/styled";

export default function Panel(props) {
  return <PanelParent>{props.children}</PanelParent>;
}

const PanelParent = styled.div`
  width: 100%;
  padding: 16px;
  border-radius: 2px;
  box-sizing: border-box;
  overflow: hidden;
  cursor: pointer;
  background-color: #ffffff;
`;
