import styled from "@emotion/styled";
import typography from "../../../../../../utils/typography";

export default function MiscDayName({ name }) {
  return <DayName className="day-name">{name}</DayName>;
}

const DayName = styled.div`
  width: 48px;
  height: 48px;
  background: #ffffff;
  ${typography.body1_m};
  color: #5b7281;
  display: flex;
  justify-content: center;
  align-items: center;

  @media screen and (max-width: 620px) {
    width: 32px;
    height: 32px;
    ${typography.body3_m};
  }
`;
